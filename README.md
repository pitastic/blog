# pitastic blog 
The pitastic blog is rendered into static files with (https://hexo.io)[hexo.io]. 
This allows us to serve the blog directly from S3. This repository contains the necessary cloudformation files to make this happen.

## Requirements
- docker
- make

## Setup
### env file
Copy the env file and insert your AWS keys etc
```
cp .env.example .env
```

### Build aws container
We use a docker container with the aws cli installed to keep our environment clean
```
make docker-image
```

### Cloudformation
We need to deploy 2 cloudformation stacks.

The first stack creates the SSL certificate with AWS ACM. Due to limitations with Cloudfront the certificate has to be created in the region *us-east-1*.
```
make cloudformation-cert
```
*Attention: ACM will send an email for certificate approval to webmaster@blog.pitastic.ch - you need to manually approve the cert for the time being*

After the certificate stack deployment has finished we can deploy the s3 and cloudfront stack.
```
make cloudformation-blog
```